#!/bin/bash

set -e
set -u

testdir=$PWD/tests
cp -R $testdir $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP

for py3ver in $(py3versions -vs)
do
    echo "Running main upstream test set with Python ${py3ver}."
    echo "TODO: Whitelist the sampler tests that don't require unsatisfied deps."
    /usr/bin/python${py3ver} -B -m pytest -v -k "not dashboard" --ignore=tests/integration_tests/ --ignore=tests/visualization_tests/ --ignore=tests/samplers_tests/ tests/
done
